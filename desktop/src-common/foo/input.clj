(ns foo.input
  (:require [play-clj.core :refer [key-code]]))

(def move-increment 5)

(def direction-codes
  {(key-code :dpad-left)  :left
   (key-code :dpad-right) :right
   (key-code :dpad-up)    :up
   (key-code :dpad-down)  :down})

(defn move [entity direction]
  (let [f    (if (contains? #{:right :up} direction) + -)
        axis (if (contains? #{:left :right} direction) :x :y)]
    (assoc entity axis (f (axis entity) move-increment))))

(def handle-input
  (fn [screen entities]
    (let [entity (first entities)
          direction (direction-codes (:key screen))]
      (if direction (move entity direction)))))
