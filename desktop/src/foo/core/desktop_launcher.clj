(ns foo.core.desktop-launcher
  (:require [foo.core :refer :all])
  (:import [com.badlogic.gdx.backends.lwjgl LwjglApplication]
           [org.lwjgl.input Keyboard])
  (:gen-class))

(defn -main
  []
  (LwjglApplication. foo-game "foo" 480 480)
  (Keyboard/enableRepeatEvents true))
